# Python Example

View official HVAC documentation: https://github.com/hvac/hvac
View official ASYNC-HVAC documentation: https://github.com/Aloomaio/async-hvac

This repository is meant to demonstrate some of the basic functionality of HashiCorp Vault. Specifically, it demonstrates storing and accessing secrets stored in HashiCorp's KV2 (key/value pair) engine. 

This repository demonstrates the following Vault functionality:

- How to connect a Python3 application to a running instance of Vault
- How to write a key/value secret to Vault
- How to read a key/value secret from Vault

---

## Python3 & PIP Installation

If you do not have Python3 and PIP installed on your machine already, please follow the instructions below to install it.

1) Connect to your Linux instance 

2) Install Python3

    - Amazon Linux
    ```bash
    sudo yum install python37 -y
    python3 --version
    # Python 3.7.3
    ```

3) Install PIP

    ```bash
    curl -O https://bootstrap.pypa.io/get-pip.py
    python3 get-pip.py --user
    pip --version
    # pip 21.1.2 from /home/ec2-user/.local/lib/python3.7/site-packages/pip (python 3.7)
    ```
---

## Add HVAC module to existing Python app

To add the HVAC or ASYNC-HVAC modules to an existing Python3 application, use pip to install one of the following modules.

- HVAC

    ```bash
    pip install hvac
    ```

- ASYNC-HVAC
    ```bash
    pip install async-hvac
    ```

---

## Configure your environment to connect to Vault

Set the following environment variables to configure which Vault server this application connects to. These variables tell your app where your instance of Vault is running, which token to use for authentication and which namespace to use.

```bash
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN='[TOKEN]'
```

---

## Create a test secrets engine

Run the following to create a test secrets engine we can read from and write to:

```
vault secrets enable --path=secret kv-v2
```

---

## Run the Python app

Install the HVAC module

```bash
pip install hvac
```

Run the Python app via the command line

```bash
 python3 vault-demo.py
```

If the app ran successfull, you should see the following output:

```bash
Writing secret: demo
{'item1': 'adp is awesome', 'item2': 'protect your secrets', 'item3': 'pythons are scary'}
Reading secret: demo
{'item1': 'adp is awesome', 'item2': 'protect your secrets', 'item3': 'pythons are scary'}
Writing secret: demo
{'item1': 'adp is awesome', 'item2': 'secrets are cool', 'item3': 'python rocks!'}
Reading secret: demo
{'item1': 'adp is awesome', 'item2': 'secrets are cool', 'item3': 'python rocks!'}
```

To validate the KV was written successfully, you can also:

- Login to the Vault UI: http://127.0.0.1:8200/ui
- Verify via the command line `vault kv get seret/demo`
