import hvac
import os

# Write a secret to a KV V2 engine
def write_secret(secret_name, secret_data):

    print("Writing secret: " + secret_name)
    print(secret_data)

    client.secrets.kv.v2.create_or_update_secret(
        path=secret_name,
        secret=secret_data
    )

# Read a secret from a KV V2 engine
def read_secret(secret_name):

    print("Reading secret: " + secret_name)   
    
    response = client.secrets.kv.read_secret_version(path=secret_name)

    print(response['data']['data'])

# Create a new client to connect to Vault. If you're using Enterprise Vault and need to configure
# a namespace, do it here.
client = hvac.Client(
    url=os.environ['VAULT_ADDR'],
    token=os.environ['VAULT_TOKEN']
    #namespace=os.environ['VAULT_NAMESPACE']
)

# Create a new secret
secret_name='demo'
secret_data=dict(
    item1='adp is awesome',
    item2='protect your secrets',
    item3='pythons are scary'
)

# Write the secret to Vault and read it back
write_secret(secret_name, secret_data)
read_secret(secret_name)

# Update the secrets values, write it, and read it back again
secret_data['item2']='secrets are cool'
secret_data['item3']='python rocks!'

write_secret(secret_name, secret_data)
read_secret(secret_name)